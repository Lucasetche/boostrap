import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js"
import { getDatabase, ref, set, push, onValue, query, orderByChild } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js' //importamos funciones que estan escritas en javascript//
//los que estan "apagados" es que no lo estamos usando//

const firebaseConfig = { //creamos objeto para vincular nuestro js con firebase//
    apiKey: "AIzaSyA8aUhoetukkwuxiNDtiOcZZJPlYVOJ-Wg",
    authDomain: "control-de-acceso-38122.firebaseapp.com",
    projectId: "control-de-acceso-38122",
    databaseURL: "https://control-de-acceso-38122-default-rtdb.firebaseio.com/",
    storageBucket: "control-de-acceso-38122.appspot.com",
    messagingSenderId: "362430641975",
    appId: "1:362430641975:web:fccbcf8743a252fb438cb2"
  };

var UID;
var correo;

//metodos para subir datos a la base: push y set
let app = initializeApp(firebaseConfig);

const database = getDatabase(app) //agarra una referencia al servicio de database//

const auth = getAuth(app);

onAuthStateChanged(auth, (user) => {
    if (user) {
      // User is signed in, see docs for a list of available properties
      // https://firebase.google.com/docs/reference/js/firebase.User
      const uid = user.uid;
      UID = uid
      console.log("uid: " + uid)


      const email = user.email;
      correo = email;  
      console.log("correo: " + correo)

    } else {
      // User is signed out
      // ...
    }
  });

let mensajeRef = document.getElementById("textoChatId")
let botonRef = document.getElementById("botonChatId")
let chatRef = document.getElementById("mensajeChatId")

botonRef.addEventListener("click", cargarmensaje)

function cargarmensaje(){
    let mensaje = mensajeRef.value;

    set(ref(database, "mensaje/"), {
        msg: mensaje     
        
    })
    mensajeRef.value = "" //esto es para borrar el mensaje en el campo
    console.log("mensaje enviado")
}

   //escucha cambios en una ubicacion especifica en la base de datos 

  const queryMensaje = query(ref(database, 'mensaje'), orderByChild('msg'));


onValue(queryMensaje,(snapshot) => {
     
  
  
  snapshot.forEach((childSnapshot) => {
       const childKey = childSnapshot.key;
       const childData = childSnapshot.val().msg;
       console.log(childData);
       console.log(childKey);
 
      console.log("lectura de data");
    

      const lectura = snapshot.val();

      chatRef.innerHTML += `
       <p>${correo}: ${lectura.msg}</p>` // el += es para que no se borren los mensaje de la nube//
    // snapshot.forEach((childSnapshot) => {
    //   const childKey = childSnapshot.key;
    //   const childData = childSnapshot.val().msg;
    //   console.log(childData);
    //   console.log(childKey);

    //   const lectura = snapshot.val(); 
     
    //   chatRef.innerHTML += `
    //  <p>${childSnapshot.val().correo}: ${lectura.msg}</p>` // el += es para que no se borren los mensaje de la nube//
  }) });
//  })